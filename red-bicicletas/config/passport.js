var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var FacebookTokenStrategy = require('passport-facebook-token');
var Usuario = require('../models/usuario');

passport.use(
  new FacebookTokenStrategy(
    {
      clientID: process.env.FACEBOOK_ID,
      clientSecret: process.env.FACEBOOK_SECRET,
    },
    (accesToken, refreshToken, profile, done) => {
      try {
        Usuario.findOneOrCreateByFacebook(profile, (err, res) => {
          if (err) console.log('err', err);
          return done(err, user);
        });
      } catch (err2) {
        console.log('err2', err2);
        return done(err2, null);
      }
    }
  )
);

passport.use(
  new LocalStrategy((username, password, done) => {
    console.log('username, password', username, password);
    Usuario.findOne({ email: username }, (err, usuario) => {
      console.log('usuario', usuario);
      if (err) {
        return done(err);
      }
      if (!usuario) {
        return done(null, false, {
          message: 'Email no existente o incorrecto.',
        });
      }
      if (!usuario.validatePassword(password)) {
        return done(null, false, { message: 'Contrasena incorrecta.' });
      }
      return done(null, usuario);
    });
  })
);

// console.log('process.env', process.env);
// console.log('process.env.HOST', process.env.HOST + '/auth/google/callback');
// console.log('process.env.GOOGLE_CLIENT_ID', process.env.GOOGLE_CLIENT_ID);
passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOGGLE_CLIENT_ID,
      clientSecret: process.env.GOGGLE_CLIENT_SECRET,
      callbackURL: process.env.HOST + '/auth/google/callback',
    },
    (accesToken, refreshToken, profile, cb) => {
      console.log('profile', profile);
      Usuario.findOneOrCreateByGoogle(profile, (err, user) => {
        return cb(err, user);
      });
    }
  )
);

passport.serializeUser((user, cb) => {
  cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
  Usuario.findById(id, (err, usuario) => {
    cb(err, usuario);
  });
});

module.exports = passport;
