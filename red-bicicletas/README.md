# Red Bicicletas 
---
Este es el resultado de las guias planteadas en el curso. Cree este archivo readme en formato .md debido a que es el estandar para crearlo.

## Clonar el proyecto 
---
Para clonar correctamente el proyecto puedes:
1. Ingresar a  mi repositorio publico de bitbucket: https://bitbucket.org/JeffreyLuna/server-test/src/master/ y descargar el proyecto o clonarlo.
2. Ejecutar el comando: git clone https://bitbucket.org/JeffreyLuna/server-test.git para clonarlo en tu repositorio local.
3. Utilizar sourcetree para administrar tus repositrios.

## Instalacion y ejecucion del proyecto  🔧
---
Ubicate en la carpeta del repositorio local donde clonaste el proyecto y ejecuta el comando: ~~~npm install~~~ de esta forma instalaras todas las dependencias necesarias para ejecutar el proyecto.

Para ejecutar el proyecto solo debes ingresar a la carpeta del proyecto: '__dirname/red-bicicletas' y6 ejecutar los comandos: 
1. ```npm start```: Para la version normal del proyecto.
2. ```npm run devstart```: Para la version dev del proyecto con la cual podremos hacer uso de nodemon y su hot reload.

## Probando el proyecto ⚙️
---
Puedes utilizar la siguiente coleccion de postman en la carpeta: red-bicicletas. (https://www.getpostman.com/collections/2d274ac0954e2e3d21c9)
Para probar los endpoints desarrollados despues de ejecutar el proyecto.

## Construido con 🛠️
---
_Se utilizaron las siguientes herramientas para llevar a cabo el proyecto:_

* [Express](https://expressjs.com/es/) - El framework web usado
* [Nodemon](https://www.npmjs.com/package/nodemon) - Tool to dev enviroment

## Autores ✒️
---
* **Jeffrey Luna** - *Trabajo Inicial* - [MrMoon](https://bitbucket.org/JeffreyLuna/server-test/src/master/red-bicicletas/)

## Gracias 🎁
---
* A la plataforma coursera por tan buen curso desarrollado
* A la gobernacion del Valle del Cauca por la oportunidad de ingresar al curso

## Reflexion - Semana 2 - Testing manual vs automático
---
Realmente es necesaria la documentacion de ambas formas, las automáticas nos dan la certificación de que lo que hemos tocado se mantiene funcionando como se programo, mientras que las manuales dan cierto entendimiento y junto con la herramienta postman se pueden probar de manera mas visual todos los contenidos, headers, etc. Ambos son necesarios, hacen parte de la documentación.
