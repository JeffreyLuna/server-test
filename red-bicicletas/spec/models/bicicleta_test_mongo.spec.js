var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");

describe("Testing Bicicletas", () => {
  beforeEach((done) => {
    var mongoDB = "mongodb://localhost/red_bicicletas";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    const db = mongoose.connection;

    db.on("error", console.error.bind(console, "Connection error"));
    db.once("open", () => {
      console.log("We are connected to test database");
      done();
    });
  });

  afterEach((done) => {
    Bicicleta.deleteMany({}, (error, success) => {
      if (error) console.log("error", error);
      done();
    });
  });

  describe("Bicicleta.createInstance", () => {
    it("Crea una instancia de Bicicleta", () => {
      var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe("verde");
      expect(bici.modelo).toBe("urbana");
      expect(bici.ubicacion[0]).toBe(-34.5);
      expect(bici.ubicacion[1]).toBe(-54.1);
    });
  });

  describe("Bicicleta.allBicis", () => {
    it("Comienza vacia", (done) => {
      Bicicleta.allBicis((error, bicis) => {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe("Bicicleta.add", () => {
    it("Agrega solo una bici", (done) => {
      var bici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
      Bicicleta.add(bici, (error, newBici) => {
        if (error) console.log("error", error);
        Bicicleta.allBicis((err, bicis) => {
          expect(bicis.length).toBe(1);
          expect(bicis[0].code).toBe(bici.code);
          done();
        });
      });
    });
  });

  describe("Bicicleta.findByCode", () => {
    it("Debe devolver la bici con code 1", (done) => {
      Bicicleta.allBicis((error, bicis) => {
        expect(bicis.length).toBe(0);

        var bici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
        Bicicleta.add(bici, (error, newBici) => {
          if (error) console.log("error", error);

          var bici1 = new Bicicleta({
            code: 2,
            color: "roja",
            modelo: "montaña",
          });
          Bicicleta.add(bici1, (error, newBici) => {
            if (error) console.log("error", error);
            Bicicleta.findByCode(1, (error, targetBici) => {
              expect(targetBici.code).toBe(bici.code);
              expect(targetBici.color).toBe(bici.color);
              expect(targetBici.modelo).toBe(bici.modelo);
              done();
            });
          });
        });
      });
    });
  });

  describe("Bicicleta.removeByCode", () => {
    it("Debe remover la bici con code 1", (done) => {
      Bicicleta.allBicis((error, bicis) => {
        expect(bicis.length).toBe(0);

        var bici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
        Bicicleta.add(bici, (error, newBici) => {
          if (error) console.log("error", error);

          var bici1 = new Bicicleta({
            code: 2,
            color: "roja",
            modelo: "montaña",
          });
          Bicicleta.add(bici1, (error, newBici) => {
            if (error) console.log("error", error);
            Bicicleta.removeByCode(1, (error, targetBici) => {
              Bicicleta.allBicis((error, bicis) => {
                expect(bicis.length).toBe(1);
                done();
              });
            });
          });
        });
      });
    });
  });
});
