var Bicicleta = require('../../models/bicicleta');

beforeEach(() => {
    Bicicleta.allBicis = [];
})

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
    });
})

describe('Bicicleta.add', () => {
    it('Agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var bici1 = new Bicicleta(1, 'rojo', 'urbana', [3.451655, -76.531921]);
        Bicicleta.add(bici1);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(bici1);
    });
})

describe('Bicicleta.findBy', () => {
    it('Debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
        var bici1 = new Bicicleta(1, 'verde', 'urbana');
        var bici2 = new Bicicleta(2, 'verde', 'urbana');

        Bicicleta.add(bici1);
        Bicicleta.add(bici2);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(bici1.id);
        expect(targetBici.color).toBe(bici1.color);
        expect(targetBici.modelo).toBe(bici1.modelo);
    });
})

describe('Bicicleta.removeById', () => {
    it('Debe remover la bicicleta con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
        var bici1 = new Bicicleta(1, 'verde', 'urbana');
        var bici2 = new Bicicleta(2, 'verde', 'urbana');

        Bicicleta.add(bici1);
        Bicicleta.add(bici2);

        Bicicleta.removeById(1);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(bici2);
    });
})

