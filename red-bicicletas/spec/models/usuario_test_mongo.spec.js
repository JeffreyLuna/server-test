var mongoose = require("mongoose");
var Usuario = require("../../models/usuario");
var Bicicleta = require("../../models/bicicleta");
var Reserva = require("../../models/reserva");
var moment = require("moment");

describe("Testing usuario", () => {
  beforeEach((done) => {
    console.log("Initializing testing...");
    var mongoDB = "mongodb://localhost/red_bicicletas";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    const db = mongoose.connection;

    db.on("error", console.error.bind(console, "Connection error"));
    db.once("open", () => {
      console.log("We are connected to test database");
      done();
    });
  });

  afterEach((done) => {
    Usuario.deleteMany({}, (error, success) => {
      if (error) console.log("error", error);
      console.log("Finishing testing...");
      done();
    });

    Reserva.deleteMany({}, (error, success) => {
      if (error) console.log("error", error);
      console.log("Finishing testing...");
      done();
    });
  });

  describe("Usuario.create", () => {
    it("Crea un nuevo usuario con un nombre", (done) => {
      var user = Usuario.createInstance("Jeffrey");
      expect(user.name).toBe("Jeffrey");

      Usuario.add(user, (error, res) => {
        if (error) console.log("error", error);
        Usuario.allUsers((error, users) => {
          if (error) console.log("error", error);
          expect(users.length).toBe(1);
          expect(users[0].name).toBe(user.name);
          done();
        });
      });
    });
  });

  describe("Usuario.reservar", () => {
    it("Crea una nueva reserva con un usuario y una bici", (done) => {
      var user = new Usuario({ name: "Jeffrey" });
      user.save();
      var bici = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "urbana",
        ubicacion: [-34.5, -54.1],
      });
      bici.save();
      // var user = Usuario.createInstance("Jeffrey");
      // var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
      var today = moment().format("YYYY-MM-DD");
      var tomorrow = moment(today, "YYYY-MM-DD")
        .add(1, "days")
        .format("YYYY-MM-DD");

      Usuario.reservar(user.id, bici.id, today, tomorrow, (error, reserva) => {
        Reserva.find({})
          .populate("bicicleta")
          .populate("usuario")
          .exec((error, reservas) => {
            // console.log("reservas", reservas);
            expect(reservas[0].usuario.name).toBe("Jeffrey");
            expect(reservas[0].bicicleta.code).toBe(1);
            done();
          });
      });
    });
  });
});
