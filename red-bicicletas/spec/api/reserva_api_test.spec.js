var Reserva = require("../../models/reserva");
var request = require("request");
var server = require("../../bin/www");
var mongoose = require("mongoose");

describe("Reserva API", () => {
  beforeEach((done) => {
    var mongoDB = "mongodb://localhost/test";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    const db = mongoose.connection;

    db.on("error", console.error.bind(console, "Connection error"));
    db.once("open", () => {
      console.log("We are connected to test database");
      done();
    });
  });

  afterEach((done) => {
    Reserva.deleteMany({}, (err, response) => {
      if (err) console.log("err", err);
      done();
    });
  });

  describe("Test reservas /", () => {
    it("Probando lista de reservas API", (done) => {
      request.get("http://localhost:5000/api/reservas/", (err, res, body) => {
        if (err) console.log("err", err);
        expect(JSON.parse(body).reservas.length).toBe(0);
        done();
      });
    });
  });
});
