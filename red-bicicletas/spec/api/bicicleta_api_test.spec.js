var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");
var mongoose = require("mongoose");

describe("Bicicleta API", () => {
  beforeEach((done) => {
    var mongoDB = "mongodb://localhost/test";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    const db = mongoose.connection;

    db.on("error", console.error.bind(console, "Connection error"));
    db.once("open", () => {
      console.log("We are connected to test database");
      done();
    });
  });

  afterEach((done) => {
    Bicicleta.deleteMany({}, (err, response) => {
      if (err) console.log("err", err);
      done();
    });
  });

  describe("Test bicicletas /", () => {
    it("Probando lista de bicicletas API", (done) => {
      request.get("http://localhost:5000/api/bicicletas/", (err, res, body) => {
        if (err) console.log("err", err);
        expect(JSON.parse(body).bicicletas.length).toBe(0);
        done();
      });
    });
  });

  describe("Test bicicletas /create", () => {
    it("Probando creacion de bicicleta", (done) => {
      var headers = { "content-type": "application/json" };
      var aBici =
        '{ "code": 1, "color": "rojo", "modelo": "urbano", "lat": -34, "lng": -54 }';
      request.post(
        {
          headers: headers,
          url: "http://localhost:5000/api/bicicletas/create",
          body: aBici,
        },
        (err, res, body) => {
          if (err) console.log("err", err);
          var requestResponse = JSON.parse(body).bicicleta;
          expect(requestResponse.code).toBe(1);
          expect(requestResponse.color).toBe("rojo");
          expect(requestResponse.modelo).toBe("urbano");
          expect(requestResponse.ubicacion[0]).toBe(-34);
          expect(requestResponse.ubicacion[1]).toBe(-54);
          done();
        }
      );
    });
  });

  describe("Test bicicletas /update", () => {
    it("Probando edicion de bicicleta", (done) => {
      var headers = { "content-type": "application/json" };
      var aBici = {
        code: 10,
        color: "rojo",
        modelo: "urbano",
        lat: -34,
        lng: -54,
      };
      var bBici = {
        code: 20,
        color: "verde",
        modelo: "montana",
        lat: -34,
        lng: -54,
      };

      Bicicleta.add(aBici);
      Bicicleta.add(bBici);

      var dataToUpdate =
        '{ "code": 20, "color": "gris", "modelo": "calle", "lat": -34, "lng": -54 }';

      request.put(
        {
          headers: headers,
          url: "http://localhost:5000/api/bicicletas/update",
          body: dataToUpdate,
        },
        (err, res, body) => {
          if (err) console.log("err", err);
          var requestResponse = JSON.parse(body).bicicleta;
          expect(requestResponse.code).toBe(20);
          expect(requestResponse.color).toBe("gris");
          expect(requestResponse.modelo).toBe("calle");
          expect(requestResponse.ubicacion[0]).toBe(-34);
          expect(requestResponse.ubicacion[1]).toBe(-54);
          done();
        }
      );
    });
  });

  describe("Test bicicletas /delete", () => {
    it("Eliminar bicicleta", (done) => {
      var headers = { "content-type": "application/json" };

      var bBici = {
        code: 20,
        color: "verde",
        modelo: "montana",
        lat: -34,
        lng: -54,
      };

      Bicicleta.add(bBici);

      request.delete(
        {
          headers: headers,
          url: "http://localhost:5000/api/bicicletas/delete",
          body: '{ "code": 20 }',
        },
        (err, res, body) => {
          if (err) console.log("err", err);
          expect(res.statusCode).toBe(204);
          done();
        }
      );
    });
  });
});
