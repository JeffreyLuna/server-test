var Usuario = require("../../models/usuario");
var request = require("request");
var server = require("../../bin/www");
var mongoose = require("mongoose");

describe("Usuario API", () => {
  beforeEach((done) => {
    var mongoDB = "mongodb://localhost/test";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    const db = mongoose.connection;

    db.on("error", console.error.bind(console, "Connection error"));
    db.once("open", () => {
      console.log("We are connected to test database");
      done();
    });
  });

  afterEach((done) => {
    Usuario.deleteMany({}, (err, res) => {
      if (err) console.log("err", err);
      done();
    });
  });

  describe("Test usuarios /", () => {
    it("Probando lista de usuarios API", (done) => {
      request.get("http://localhost:5000/api/usuarios/", (err, res, body) => {
        if (err) console.log("err", err);
        expect(JSON.parse(body).usuarios.length).toBe(0);
        done();
      });
    });
  });

  describe("Test usuarios /create", () => {
    it("Probando creacion de usuario", (done) => {
      var headers = { "content-type": "application/json" };
      var user = '{ "name": "MrMoonTest" }';
      request.post(
        {
          headers: headers,
          url: "http://localhost:5000/api/usuarios/create",
          body: user,
        },
        (err, res, body) => {
          if (err) console.log("err", err);
          var requestResponse = JSON.parse(body).usuario;
          expect(requestResponse.name).toBe("MrMoonTest");
          done();
        }
      );
    });
  });

  describe("Test usuarios /update", () => {
    it("Probando edicion de usuario", (done) => {
      var headers = { "content-type": "application/json" };
      var aUser = {
        name: "MrMoon1",
      };
      var bUser = {
        name: "MrMoon2",
      };

      Usuario.add(aUser);
      Usuario.add(bUser);

      var dataToUpdate = '{ "prev_name": "MrMoon2", "new_name": "MrMoonNew" }';

      request.put(
        {
          headers: headers,
          url: "http://localhost:5000/api/usuarios/update",
          body: dataToUpdate,
        },
        (err, res, body) => {
          if (err) console.log("err", err);
          var requestResponse = JSON.parse(body).usuario;
          expect(requestResponse.name).toBe("MrMoonNew");
          done();
        }
      );
    });
  });

  describe("Test usuarios /remove", () => {
    it("Eliminar usuario", (done) => {
      var headers = { "content-type": "application/json" };

      var bUser = {
        name: "MrMoonTest",
      };

      Usuario.add(bUser);

      request.delete(
        {
          headers: headers,
          url: "http://localhost:5000/api/usuarios/delete",
          body: '{ "name": "MrMoonTest" }',
        },
        (err, res, body) => {
          if (err) console.log("err", err);
          expect(res.statusCode).toBe(204);
          done();
        }
      );
    });
  });
});
