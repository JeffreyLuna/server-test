var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: { type: '2dsphere', sparse: true },
  },
});

bicicletaSchema.methods.toString = () => {
  return `code: ${this.code} | color: ${this.color}`;
};

bicicletaSchema.statics.allBicis = function (cb) {
  //Parametros de filtrado y callback
  //   console.log("this.find({})", this.find({}));
  return this.find({}, cb);
};

bicicletaSchema.statics.createInstance = (code, color, modelo, ubicacion) => {
  return {
    code: code,
    color: color,
    modelo: modelo,
    ubicacion: ubicacion,
  };
};

bicicletaSchema.statics.add = async function (bici, cb) {
  await this.create(bici, cb);
};

bicicletaSchema.statics.findByCode = function (code, cb) {
  return this.findOne({ code }, cb);
};

// bicicletaSchema.statics.update = function (bici, cb) {
//   return this.findAndModify({
//     query: {
//       code: bici.code,
//       color: bici.color,
//       modelo: bici.modelo,
//       ubicacion: bici.ubicacion,
//     },
//     sort: { rating: 1 },
//     update: { $inc: { score: 1 } },
//   });
// };

bicicletaSchema.statics.removeByCode = function (code, cb) {
  return this.deleteOne({ code }, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);
