var mongoose = require("mongoose");
var Schema = mongoose.Schema;

const moment = require("moment");

var reservaSchema = new Schema({
  fechaInicial: Date,
  fechaFinal: Date,
  bicicleta: { type: Schema.Types.ObjectId, ref: "Bicicleta" },
  usuario: { type: Schema.Types.ObjectId, ref: "Usuario" },
});

reservaSchema.methods.diasReserva = function () {
  return moment(this.fechaFinal).diff(moment(fechaInicial), "days") + 1;
};

reservaSchema.statics.allReservas = function (cb) {
  return this.find({}, cb).populate("bicicleta").populate("usuario");
};

reservaSchema.statics.removeAll = function (cb) {
  return this.deleteMany({}, cb);
};

module.exports = mongoose.model("Reserva", reservaSchema);
