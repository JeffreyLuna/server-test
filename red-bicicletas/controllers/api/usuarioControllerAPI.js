var moment = require('moment');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');

exports.usuarios_list = async (req, res) => {
  res.status(200).json({
    usuarios: await Usuario.allUsers(),
  });
};

// exports.usuario_create = (req, res) => {
//   var usuario = Usuario.createInstance(req.body.name);
//   Usuario.add(usuario, (error, usuario) => {
//     if (error) console.log("error", error);
//     res.status(200).json({
//       code: 200,
//       usuario,
//     });
//   });
// };

exports.usuario_create = async (req, res) => {
  var usuario = new Usuario({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
  });
  await usuario.save((err) => {
    if (err) return res.status(500).json(err);
    res.status(200).json(usuario);
  });
};

exports.usuario_update = async (req, res) => {
  var userFinded = await Usuario.findByName(req.body.prev_name);
  userFinded.name = req.body.new_name;
  userFinded.save();
  res.status(200).json({ usuario: userFinded });
};

exports.usuario_delete = (req, res) => {
  Usuario.removeByName(req.body.name, (err, response) => {
    if (err) console.log('err', err);
    res.status(204).send();
  });
};

exports.usuario_reservar = (req, res) => {
  var fechaInicial = moment(req.body.fecha_inicial).format('YYYY-MM-DD');
  var fechaFinal = moment(req.body.fecha_final).format('YYYY-MM-DD');

  Usuario.reservar(
    req.body.user_id,
    req.body.bici_id,
    fechaInicial,
    fechaFinal,
    (error, reserva) => {
      console.log('reserva', reserva);
      Reserva.find({})
        .populate('bicicleta')
        .populate('usuario')
        .exec((error, reservas) => {
          if (error) console.log('error', error);
          res.status(200).json({
            code: 200,
            data: { message: 'Reserva creada', reserva: reservas[0] },
          });
        });
    }
  );
};
