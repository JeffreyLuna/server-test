var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = async function (req, res) {
  res.status(200).json({
    bicicletas: await Bicicleta.allBicis(),
  });
};

exports.bicicleta_create = async (req, res) => {
  console.log('req.body', req.body);

  var bici = new Bicicleta({
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.lat, req.body.lng],
  });

  await bici.save((err) => {
    if (err) return res.status(500).json(err);
    res.status(200).json(usuario);
  });
};

exports.bicicleta_update = async (req, res) => {
  var biciFinded = await Bicicleta.findByCode(req.body.code);
  biciFinded.color = req.body.color;
  biciFinded.modelo = req.body.modelo;
  biciFinded.ubicacion = [req.body.lat, req.body.lng];
  biciFinded.save();
  res.status(200).json({ bicicleta: biciFinded });
};

exports.bicicleta_delete = (req, res) => {
  Bicicleta.removeByCode(req.body.code, (err, response) => {
    if (err) console.log('err', err);
    res.status(204).send();
  });
};
