const BCrypt = require('bcrypt');
const JWT = require('jsonwebtoken');
const Usuario = require('../../models/usuario');

module.exports = {
  authenticate: (req, res, next) => {
    Usuario.findOne({ email: req.body.email }, (err, usuario) => {
      if (err) {
        next(err);
      } else {
        if (usuario === null) {
          return res.status(401).json({
            status: 'Error',
            message: 'No hay un usuario con el email ingresado.',
          });
        }
        if (
          usuario != null &&
          BCrypt.compareSync(req.body.password, usuario.password)
        ) {
          //   usuario.save((err, usuario) => {
          const token = JWT.sign(
            { id: usuario._id },
            req.app.get('secretKey'),
            { expiresIn: '7d' }
          );
          res.status(200).json({
            message: 'Usuario encontrado!',
            data: { usuario, token },
          });
          //   });
        } else {
          res.status(401).json({
            status: 'Error',
            message: 'Email o password invalido',
            data: null,
          });
        }
      }
    });
  },

  forgotPassword: (req, res, next) => {
    Usuario.findOne({ email: req.body.email }, (err, usuario) => {
      if (!usuario)
        return res
          .status(401)
          .json({ message: 'No existe el usuario', data: null });

      //   usuario.resetPassword((err) => {
      //     if (err) {
      //       return next(err);
      //     }
      //     res
      //       .status(200)
      //       .json({
      //         message: 'Se envio un email para restablecer el password',
      //         data: null,
      //       });
      //   });
    });
  },

  authFacebookToken: (req, res, next) => {
    if (req.user) {
      req.user
        .save()
        .then(() => {
          const token = JWT.sign(
            { id: req.user.id },
            req.app.get('secretKey'),
            {
              expiresIn: '7d',
            }
          );
          res.status(200).json({
            message: 'Usuario encontrado o creado',
            data: { user: req.user, token },
          });
        })
        .catch((err) => {
          console.log('err', err);
          res.status(500).json({ message: err.message });
        });
    }
  },
};
