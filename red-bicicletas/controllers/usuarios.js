var Usuario = require("../models/usuario");

module.exports = {
  list: (req, res, next) => {
    Usuario.find({}, (err, usuarios) => {
      res.render("usuarios/index", { usuarios });
    });
  },

  update_get: (req, res, next) => {
    Usuario.findById(req.params.id, (err, usuario) => {
      res.render("usuarios/update", { errors: {}, usuario });
    });
  },

  update: (req, res, next) => {
    var update_values = { name: req.body.name };
    Usuario.findByIdAndUpdate(req.params.id, update_values, (err, usuario) => {
      if (err) {
        console.error("err", err);
        res.render("/usuarios/update", {
          errors: err.errors,
          usuario: new Usuario({ name: req.body.name, email: req.body.email }),
        });
      } else {
        res.redirect("/usuarios");
        return;
      }
    });
  },

  create_get: (req, res, next) => {
    res.render("usuarios/create", { errors: {}, usuario: new Usuario() });
  },

  create: (req, res, next) => {
    if (req.body.password != req.body.confirm_password) {
      res.render("usuarios/create", {
        errors: {
          confirm_password: { message: "Las contrasenas no coinciden" },
        },
        usuario: new Usuario({ name: req.body.name, email: req.body.email }),
      });
      return;
    }

    Usuario.create(
      {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
      },
      (err, newUser) => {
        if (err) {
          res.render("usuarios/create", {
            errors: err.errors,
            usuario: new Usuario({
              name: req.body.name,
              email: req.body.email,
              password: req.body.password,
            }),
          });
        } else {
          newUser.sendWelcomeEmail();
          res.redirect("/usuarios");
        }
      }
    );
  },

  delete: (req, res, next) => {
    Usuario.findByIdAndDelete(req.body.id, (err) => {
      if (err) {
        next(err);
      } else {
        res.redirect("/usuarios");
      }
    });
  },
};
