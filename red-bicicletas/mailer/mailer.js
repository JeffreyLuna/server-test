var nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');
// email sender function

// // Definimos el transporter
// const transporter = nodemailer.createTransport({
//   host: 'smtp.ethereal.email',
//   port: 587,
//   auth: {
//     user: 'delores.kessler@ethereal.email',
//     pass: 'ufxXswBvPDW8khTZsP',
//   },
// });

let emailConfig;
if (process.env.NODE_ENV == 'production') {
  const options = {
    auth: {
      api_key: process.env.SENDGRID_API_SECRET_KEY,
    },
  };
  emailConfig = sgTransport(options);
} else if (process.env.NODE_ENV == 'stagging') {
  console.log('Youre in Staggin server');
  const options = {
    auth: {
      api_key: process.env.SENDGRID_API_SECRET_KEY,
    },
  };
  emailConfig = sgTransport(options);
} else {
  emailConfig = {
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
      user: process.env.ETHEREAL_USER,
      pass: process.env.ETHEREAL_PASSWORD,
    },
  };
}

module.exports = nodemailer.createTransport(emailConfig);
